<?php
$title = 'Galeria de Fotos';
$description = 'Embalagens para vestuário, Embalagens para alimentos, Envelopes, Sacolas, Sacos de lixo, Sacos Zip';
$keywords = 'Embalagens para vestuário, Embalagens para alimentos, Envelopes, Sacolas, Sacos de lixo, Sacos Zip';
include 'includes/head.php';
include 'includes/header.php';
?>

<section class="section section-light border-0 py-4">
	<div class="container">
		<div class="heading heading-border heading-middle-border heading-middle-border-center">
			<!-- <h2 class="font-weight-normal text-primary">Título</h2> -->
		</div>


		<?php $qntPag = "contGaleria"; include 'includes/galeria.php' ;?>
	</div>
</section>

<?php include 'includes/footer.php' ;?>