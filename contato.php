<?php
$title = 'Contato';
$description = 'Preencha o formulário abaixo, e aguarde nosso breve retorno.';
$keywords = 'Contato, Formulário, MamaPlast';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-0">
    <div class="row mb-5">
        <div class="col-md-7">
            <?php include 'includes/form-contato.php'; ?>
        </div>
        <div class="col-md-5">
            <div class="heading heading-border heading-middle-border heading-middle-border-center">
                <h2 class="font-weight-normal text-primary">Mais Informações</h2>
            </div>
            <ul class="list list-icons list-icons-style-3 mt-2">
                <li><i class="fas fa-map-marker-alt top-6"></i> <a target="_blank" href="<?=$linkHorario;?>" title="Veja a <?=$nomeEmpresa;?> no Google Maps!"> <?=$endereco;?> - <?=$cidade;?> <br /> CEP: <?=$cep;?></a></li>
                
                <?php
                /* Tel 1 */
                echo isset($tel) && ($tel != '') ? '<li><i class="fas fa-phone top-6"></i> <a href="'.$tellink.'" title="Telefone para Contato">'.$ddd.' '.$tel.'</a></li>' : '';
                /* Tel 2 */
                echo isset($tel2) && ($tel2 != '') ? '<li><i class="fas fa-phone top-6"></i> <a href="'.$tel2link.'" title="Telefone para Contato">'.$ddd.' '.$tel2.'</a></li>' : '';
                /* Whatsapp */
                echo isset($whats) && ($whats != '') ? '<li><i class="fab fa-whatsapp top-6"></i> <a href="'.$whatslink.'" title="Atendimento Online">'.$ddd.' '.$whats.'</a></li>' : '';
                /* E-mail */
                echo isset($email) && ($email != '') ? '<li><i class="fas fa-envelope top-6"></i> <a href="mailto:'.$email.'" title="Enviar e-mail para: '.$email.'">'.$email.'</a></li>' : '';
                /* Facebook */
                echo isset($linkFace) && ($linkFace != '') ? '<li><i class="fab fa-facebook-f top-6"></i> <a href="'.$linkFace.'" target="_blank" title="Facebook - '.$nomeEmpresa.'">facebook.com/mamaplast</a></li>' : '';
                /* Instagram */
                echo isset($linkInstagram) && ($linkInstagram != '') ? '<li><i class="fab fa-instagram"></i><a href="'.$linkInstagram.'" target="_blank" title="Instagram - '.$nomeEmpresa.'">instagram.com/mamaplast</a></li>' : '';
                ?>
            </ul>
        </div>
    </div>
</div>
<?php include 'includes/footer.php' ;?>
