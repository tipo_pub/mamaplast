<?php
$title = 'Institucional';
$description = 'Desde 1988 a MamaPlast é tradicional e referência no segmento de embalagens flexíveis.';
$keywords = 'Embalagens para alimentos coextrusados, Embalagens para alimentos congelados, Embalagens para produtos hospitalares, Embalagens para produtos químicos, Embalagens personalizadas para lavanderia, Embalagens personalizadas para produtos congelados';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4 my-5">
	<div class="row align-items-center justify-content-center">
		<div class="col-lg-6 pb-sm-4 pb-lg-0 pr-lg-5 mb-sm-5 mb-lg-0"  data-appear-animation="fadeIn" data-appear-animation-delay="0">
			<h2 class="text-color-dark font-weight-normal text-6 mb-2">A <strong class="font-weight-extra-bold">Empresa</strong></h2>
			<p class="lead">Desde 1988 a MamaPlast é tradicional e referência no segmento de embalagens flexíveis. Com uma moderna estrutura e serviços de primeira linha, a MamaPlast oferece alta qualidade em embalagens PP, PEAD, COEX e Biodegradáveis, atendendo diversos segmentos, tais como: Industriais, Automotivos, Alimenticios, Farmacêuticos, Metalúrgicos, Laboratórios, entre outros. Além disso, ainda oferecemos atendimento exclusivo e personalizado, com o melhor prazo de entrega e ótimas facilidades de pagamento.</p>
		</div>
		<div class="col-lg-6 pb-sm-4 pb-lg-0 pr-lg-5 mb-sm-5 mb-lg-0" data-appear-animation="fadeIn" data-appear-animation-delay="0">
			<img class="img-fluid box-shadow-3 my-2 border-radius" src="<?=$pastaImg?>img-empresa-home.jpg" alt="<?=$nomeEmpresa;?>">
			
		<!-- 	<img src="<?=$caminhoGaleria?>thumbs/galeria-01.jpg" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1000" style="top: 10%; left: -50%;" alt="<?=$nomeEmpresa?>" />
			<img src="<?=$caminhoGaleria?>thumbs/galeria-01.jpg" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="500" style="top: -33%; left: -29%;" alt="<?=$nomeEmpresa?>" />
			<img src="<?=$caminhoGaleria?>thumbs/galeria-01.jpg" class="img-fluid position-relative appear-animation mb-2" data-appear-animation="expandIn" data-appear-animation-delay="1500" alt="<?=$nomeEmpresa?>" /> -->
		</div>
	</div>
</div>


<!-- <div class="container py-4 my-5">
	<div class="row justify-content-center">
		<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
			<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
				<div class="featured-box">
					<div class="box-content px-lg-4 px-xl-5 py-lg-5">
						<i class="icon-featured fab fa-readme text-color-primary"></i>
						<h3 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Portfólio</strong></h3>
						<p class="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit. </p>
						<a href="<?=$url?>portfolio" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 text-uppercase mt-3" title="Saiba Mais sobre nosso Portfólio">Saiba Mais</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
			<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
				<div class="featured-box">
					<div class="box-content px-lg-4 px-xl-5 py-lg-5">
						<i class="icon-featured far fa-handshake text-color-primary"></i>
						<h3 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Parceiros</strong></h3>
						<p class="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit. </p>
						<a href="<?=$url?>parceiros" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 text-uppercase mt-3" title="Saiba Mais sobre nossos Parceiros">Saiba Mais</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-10 col-md-7 col-lg-4 mb-4 mb-lg-0">
			<div class="featured-boxes featured-boxes-modern-style-2 featured-boxes-modern-style-2-hover-only featured-boxes-modern-style-primary my-4">
				<div class="featured-box">
					<div class="box-content px-lg-4 px-xl-5 py-lg-5">
						<i class="icon-featured fas fa-users text-color-primary"></i>
						<h3 class="font-weight-normal text-5"><strong class="font-weight-extra-bold">Clientes</strong></h3>
						<p class="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit. </p>
						<a href="<?=$url?>clientes" class="btn btn-light btn-outline font-weight-semibold text-color-dark btn-px-5 btn-py-2 border-width-1 text-1 text-uppercase mt-3" title="Saiba Mais sobre nossos Clientes">Saiba Mais</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 -->
<?php include 'includes/footer.php' ;?>