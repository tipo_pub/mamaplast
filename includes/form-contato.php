            <form role="form" method="post">
                <div class="heading heading-border heading-middle-border heading-middle-border-center">
                    <h2 class="font-weight-normal text-primary">Formulário de Contato</h2>
                </div>

                <p class="lead">Preencha o formulário abaixo, e aguarde nosso breve retorno.</p>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nome*" required name="nome" value="<?php echo isset($_POST['nome']) && !empty($_POST['nome']) ? $_POST['nome'] : '';?>" />
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <input type="email" class="form-control" placeholder="E-mail*" required name="email" value="<?php echo isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : '';?>" />
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control input-phone" placeholder="Telefone" name="telefone" pattern=".{15,16}" required title="Insira um número de celular ou telefone." value="<?php echo isset($_POST['telefone']) && !empty($_POST['telefone']) ? $_POST['telefone'] : '';?>" />
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="4" placeholder="Mensagem*" required name="mensagem"><?php echo isset($_POST['mensagem']) && !empty($_POST['mensagem']) ? $_POST['mensagem'] : '';?></textarea>
                </div>
                <div class="form-group col-md-7" style="float: left;">
                    <div class="g-recaptcha" data-sitekey="<?=$sitekey?>"></div>
                </div>
                <div class="form-group col-md-5 text-right p-0" style="line-height: 78px; float: right;">
                    <button type="submit" name="submit" value="submit" class="tp-caption btn btn-primary text-uppercase font-weight-bold">Enviar</button>
                </div>
                <?php if(isset($_POST['submit'])){ require_once('php/cadastro-form.php'); } ?>
            </form>