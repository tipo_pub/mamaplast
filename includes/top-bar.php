<div class="header-top header-top-borders header-top-light-2-borders">
	<div class="container h-100">
		<div class="header-row h-100">
			<div class="header-column justify-content-start">
				<div class="header-row">
					<nav class="header-nav-top">
						<ul class="nav nav-pills py-2">
							<?php echo isset($tel) && ($tel != '') ? '<li class="nav-item"><a href="'.$tellink.'"><i class="fas fa-phone icone-invertido"></i> <span>'.$ddd.' '.$tel.'</span></a></li>' : ''; ?>
							
							<?php echo isset($tel2) && ($tel2 != '') ? '<li class="nav-item"><a href="'.$tel2link.'"><i class="fas fa-phone icone-invertido"></i> <span>'.$ddd.' '.$tel2.'</span></a></li>' : ''; ?>
							
							<?php echo isset($whats) && ($whats != '') ? '<li class="nav-item"><a href="'.$whatslink.'"><i class="fab fa-whatsapp"></i> <span>'.$ddd.' '.$whats.'</span></a></li>' : ''; ?>
							
							<?php echo isset($email) && ($email != '') ? '<li class="nav-item"><a href="mailto:'.$email.'"><i class="fa fa-envelope"></i> <span>'.$email.'</span></a></li>' : ''; ?>
							
							<?php echo isset($linkHorario) && ($linkHorario != '') && isset($horario) && ($horario != '') ? '<li class="nav-item"><a href="'.$linkHorario.'"><i class="fa fa-clock"></i> <span>'.$horario.'</span></a></li>' : ''; ?>
						</ul>
					</nav>
				</div>
			</div>
			<div class="header-column justify-content-end">
				<div class="header-row">
					<nav class="header-nav-top">
						<ul class="header-social-icons social-icons social-icons-clean">
							<?php echo isset($linkFace) && ($linkFace != '') ? '<li class="social-icons-facebook"><a href="'.$linkFace.'" target="_blank" title="Facebook - '.$nomeEmpresa.'"><i class="fab fa-facebook-f"></i></a></li>' : ''; ?>
							
							<?php echo isset($linkInstagram) && ($linkInstagram != '') ? '<li class="social-icons-instagram"><a href="'.$linkInstagram.'" target="_blank" title="Instagram - '.$nomeEmpresa.'"><i class="fab fa-instagram"></i></a></li>' : ''; ?>
							
							<?php echo isset($linkTwitter) && ($linkTwitter != '') ? '<li class="social-icons-twitter"><a href="'.$linkTwitter.'" target="_blank" title="Twitter - '.$nomeEmpresa.'"><i class="fab fa-twitter"></i></a></li>' : ''; ?>
							
							<?php echo isset($linkedIn) && ($linkedIn != '') ? '<li class="social-icons-linkedin"><a href="'.$linkedIn.'" target="_blank" title="LinkedIn - '.$nomeEmpresa.'"><i class="fab fa-linkedin"></i></a></li>' : ''; ?>
							
							<?php echo isset($linkYoutube) && ($linkYoutube != '') ? '<li class="social-icons-youtube"><a href="'.$linkYoutube.'" target="_blank" title="Youtube - '.$nomeEmpresa.'"><i class="fab fa-youtube"></i></a></li>' : ''; ?>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
