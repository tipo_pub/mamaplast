<div class="sort-destination-loader-showing mt-4 pt-2">
    
    <h4 class="mb-5 text-6">Cases <strong>Relacionados</strong></h4>
    
    <div class="row portfolio-list sort-destination" data-sort-id="portfolio">

        <div class="col-md-3 mb-4 isotope-item brands">
            <div class="portfolio-item">
                <a href="manutencao">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/manutencao.jpg" class="img-fluid border-radius-0" alt="Manutenção">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Manutenção</span>
                                <span class="thumb-info-type">Serviços</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-md-3 mb-4 isotope-item brands">
            <div class="portfolio-item">
                <a href="engenharia-consultoria">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/consultoria.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Engenharia & Consultoria</span>
                                <span class="thumb-info-type">Serviços</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-md-3 mb-4 isotope-item logos">
            <div class="portfolio-item">
                <a href="projetos-desenvolvidos">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/projetos-desenvolvidos.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Projetos desenvolvidos</span>
                                <span class="thumb-info-type">Serviços</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <div class="col-md-3 mb-4 isotope-item logos">
            <div class="portfolio-item">
                <a href="assistencia-tecnica">
                    <span class="thumb-info thumb-info-lighten border-radius-0">
                        <span class="thumb-info-wrapper border-radius-0">
                            <img src="img/assistencia-tecnica.jpg" class="img-fluid border-radius-0" alt="">

                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Assistência Técnica</span>
                                <span class="thumb-info-type">Serviços</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon bg-green opacity-8"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
