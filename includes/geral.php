<?php
ini_set('display_errors', 2);
error_reporting(E_ALL);
$pastaEPagina = explode("/", $_SERVER['PHP_SELF']);
$pastaDominio = "";
for ($i = 0; $i < count($pastaEPagina); $i++) {
	if (substr_count($pastaEPagina[$i], ".") == 0) {
		$pastaDominio .= $pastaEPagina[$i] . "/";
	}
}

include "includes/functions.php";

$http = ($_SERVER['HTTP_HOST'] == "localhost") || ($_SERVER['HTTP_HOST'] == "www.tipotemporario.com.br") || ($_SERVER['HTTP_HOST'] == "tipotemporario.com.br") || (empty($_SERVER['HTTPS'])) ? "http://" : "https://" ;
$url = $http . $_SERVER['HTTP_HOST'] . $pastaDominio;

$infoRobots = ('localhost' == $_SERVER['HTTP_HOST']) || ('www.tipotemporario.com.br' == $_SERVER['HTTP_HOST']) || ('tipotemporario.com.br' == $_SERVER['HTTP_HOST']) ? "noindex, nofollow" : "index, follow" ;

$nomeEmpresa 		= 'MAMAPlast';
$slogan 			= '';
$author 			= 'https://www.tipopublicidade.com.br/';
$creditos 			= 'Tipo Publicidade';
$ramo 				= 'Embalagens Plásticas';
$email 				= 'contato@mamaplast.com.br';
$ddd				= '11';
$prestadora			= '(021)';
$tel 				= '4441-2020';
$tel2 				= '';
$tel3 				= '';
$whats 				= '';
$tellink 			= (isset($tel)) ? "tel:".preg_replace('/\(|\)|-| /', '', $prestadora.$ddd.$tel) : '';
$tel2link 			= (isset($tel2)) ? "tel:".preg_replace('/\(|\)|-| /', '', $prestadora.$ddd.$tel2) : '';
$tel3link 			= (isset($tel3)) ? "tel:".preg_replace('/\(|\)|-| /', '', $prestadora.$ddd.$tel3) : '';
$whatslink 			= (isset($whats)) ? "https://api.whatsapp.com/send?1=pr_BR&phone=55".preg_replace('/\(|\)|-| /', '', $ddd.$whats) : '';
$endereco 			= 'Rod. Presidente Tancredo A. Neves';
$bairro 			= '';
$cidade 			= 'Caieiras/SP';
$cep 				= '07700-000';
$pastaImg 			= $url . 'img/';
$logo 				= $pastaImg . 'logo.png';
$logoseo 			= $pastaImg . 'logoseo';
$logoFooter			= $pastaImg . 'logo-footer.png';
$logoTipoPub 		= $pastaImg . 'tipo.png';
$imgNaoExiste		= $pastaImg . 'img-nao-existe.png';
$favicon 			= $pastaImg . 'favicon.ico';
$faviconApple		= $pastaImg . 'favicon.png';
$googleTransparenci = $pastaImg . 'cadeado.png';
$reposinator 		= $pastaImg . 'celular.png';
$caminhoBanners 	= $pastaImg . 'banner/';
$caminhoGaleria 	= $pastaImg . 'galeria/';
$pastaParceClient 	= $pastaImg . 'parceiros-clientes/';
$caminhoParceiros 	= $pastaParceClient . 'parceiros/';
$caminhoClientes 	= $pastaParceClient . 'clientes/';
$caminhoProdutos 	= $pastaImg . 'produtos/';
$produtosThumbs 	= $caminhoProdutos . 'thumbs/';
$caminhoPortfolio 	= $pastaImg . 'portfolio/';
$caminhoEmpresa 	= $pastaImg . 'empresa/';
$caminhoPalavras 	= $pastaImg . 'embalagens-plasticas/';
$caminhoThumbs		= $caminhoPalavras . 'thumbs/';

$urlPagina 			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($urlPagina);
$linkPagina			= str_replace('.php', '', $urlPagina);
$canonical 			= $url . $linkPagina;
$imagem 			= str_replace('.php', '.jpg', $urlPagina); //imagem das páginas das palavras-chave
$bannerTopo 		= str_replace('.jpg', '', $imagem);

$title 				= (isset($title)) ? $title : '';
$description 		= (isset($description)) ? $description : '';
$keywords 			= (isset($keywords)) ? $keywords : '';

$AtividadesEmpresa          = 'Outros Serviços';
$urlAtividadesEmpresa       = 'mapa-site.php';

$horario			= "Seg-Sex: 9:00 - 17:00";

$interno 			= false;

//redes sociais
$linkFace 			= 'https://www.facebook.com/mamaplast';
$linkInstagram 		= 'https://www.instagram.com/mamaplast1988/';
$linkTwitter 		= '';
$linkedIn 			= '';
$linkYoutube 		= '';
$linkGoogle 		= '';
$UserTwitter 		= '';
$CodFanpage 		= '';

$linkIframeMapa		= 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3662.4605096264954!2d-46.75025520164571!3d-23.37155214583991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cee363ace07713%3A0x2d5bc7eab5c719c7!2sMamaplast%20Ind%C3%BAstria%20e%20Com%C3%A9rcio!5e0!3m2!1spt-BR!2sus!4v1567429877907!5m2!1spt-BR!2sus';
$linkHorario		= 'https://goo.gl/maps/5j1F95b8gmnobe82A';


//ReCaptcha Google
$sitekey = "6LfWELEUAAAAAE-Z5bzfKbTul3XdS86WaWkRYw3Q";
$secret_key = '6LfWELEUAAAAAH28-ip4BQ5B0lZ0u_0MYOf-Drcg';


/*--------------------------------------------------------------
- Utilizar link abaixo para obter a Latitude e Longitude do endereço do cliente

* http://www.geo-tag.de/generator/en.html *
--------------------------------------------------------------*/
$geolocation = '
<meta name="geo.region" content="BR-SP" />
<meta name="geo.placename" content="Caieiras" />
<meta name="geo.position" content="-23.37031;-46.751331" />
<meta name="ICBM" content="-23.37031, -46.751331" />
';

// Quantidade de tags nas páginas
$qntTags 			= "15";

// Quantidade de thumbs nas páginas
$qntThumbsCarrossel = "8";

// Configurações necessárias para o MENU!!!
$mapaSite 			= false;
$menuRodape 		= false;

/* -- Listagem de Palavras-Chave -- */

include "includes/lista-links-palavras.php";