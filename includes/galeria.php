<input type='hidden' id='current_page' />
<input type='hidden' id='show_per_page' />

<div class="row <?=$qntPag?> lightbox" id='content' data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
	
	<!-- Thumbnail images -->

	<!-- Empresas img -->


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-vestuario.jpg" title="Embalagem para Vestuário">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-vestuario.jpg" alt="Embalagem para Vestuário">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-alimentos-01.jpg" title="Embalagem para Alimentos - 01">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-alimentos-01.jpg" alt="Embalagem para Alimentos - 01">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-alimentos-02.jpg" title="Embalagem para Alimentos - 02">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-alimentos-02.jpg" alt="Embalagem para Alimentos - 02">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-alimentos-03.jpg" title="Embalagem para Alimentos - 03">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-alimentos-03.jpg" alt="Embalagem para Alimentos - 03">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-alimentos-04.jpg" title="Embalagem para Alimentos - 04">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-alimentos-04.jpg" alt="Embalagem para Alimentos - 04">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-coex-seguranca.jpg" title="Envelope COEX Segurança">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-coex-seguranca.jpg" alt="Envelope COEX Segurança">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelopes-segurancas-impressos.jpg" title="Envelopes Seguranças Impressos">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelopes-segurancas-impressos.jpg" alt="Envelopes Seguranças Impressos">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelopes-datados-seriados-01.jpg" title="Envelopes Datados e Seriados - 01">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelopes-datados-seriados-01.jpg" alt="Envelopes Datados e Seriados - 01">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelopes-datados-seriados-02.jpg" title="Envelopes Datados e Seriados - 02">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelopes-datados-seriados-02.jpg" alt="Envelopes Datados e Seriados - 02">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelopes-datados-seriados-03.jpg" title="Envelopes Datados e Seriados - 03">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelopes-datados-seriados-03.jpg" alt="Envelopes Datados e Seriados - 03">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-01.jpg" title="Envelope Segurança - 01">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-01.jpg" alt="Envelope Segurança - 01">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-02.jpg" title="Envelope Segurança - 02">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-02.jpg" alt="Envelope Segurança - 02">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-03.jpg" title="Envelope Segurança COEX - 03">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-03.jpg" alt="Envelope Segurança COEX - 03">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-04.jpg" title="Envelope Segurança COEX - 04">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-04.jpg" alt="Envelope Segurança COEX - 04">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-05.jpg" title="Envelope Segurança - 05">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-05.jpg" alt="Envelope Segurança - 05">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-06.jpg" title="Envelope Segurança - 06">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-06.jpg" alt="Envelope Segurança - 06">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-07.jpg" title="Envelope Segurança - 07">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-07.jpg" alt="Envelope Segurança - 07">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-datados-coex-01.jpg" title="Envelope Segurança - 01">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-datados-coex-01.jpg" alt="Envelope Segurança - 01">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-datados-coex-02.jpg" title="Envelope Segurança - 02">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-datados-coex-02.jpg" alt="Envelope Segurança - 02">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-seguranca-datados-coex-03.jpg" title="Envelope Segurança - 03">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-seguranca-datados-coex-03.jpg" alt="Envelope Segurança - 03">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacolas-01.jpg" title="Sacolas - 01">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacolas-01.jpg" alt="Sacolas - 01">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacolas-02.jpg" title="Sacolas - 02">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacolas-02.jpg" alt="Sacolas - 02">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacolas-03.jpg" title="Sacolas - 03">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacolas-03.jpg" alt="Sacolas - 03">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacolas-04.jpg" title="Sacolas - 04">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacolas-04.jpg" alt="Sacolas - 04">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacolas-05.jpg" title="Sacolas - 05">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacolas-05.jpg" alt="Sacolas - 05">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacos-lixo.jpg" title="Sacos de Lixo">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacos-lixo.jpg" alt="Sacos de Lixo">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/saco-zip-01.jpg" title="Sacos ZIP - 01">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/saco-zip-01.jpg" alt="Sacos ZIP - 01">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/saco-zip-04.jpg" title="Sacos ZIP - 02">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/saco-zip-04.jpg" alt="Sacos ZIP - 02">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/saco-zip-02.jpg" title="Sacos Metalizados Solda Pouch">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/saco-zip-02.jpg" alt="Sacos Metalizados Solda Pouch">
		</a>	
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/saco-zip-03.jpg" title="Sacos ZIP Metalizados">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/saco-zip-03.jpg" alt="Sacos ZIP Metalizados">
		</a>	
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas.jpg" alt="Empresa" />
		</a>
	</div>

	<!-- Empresas img -->

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-para alimento-solda-standup-pouch.jpg" title="Embalagem para Alimento Solda StandUp Pouch">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-para alimento-solda-standup-pouch.jpg" alt="Embalagem para Alimento Solda StandUp Pouch" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-para-alimentos.jpg" title="Embalagem para alimentos">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-para-alimentos.jpg" alt="Embalagem para alimentos" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-para-alimentos-02.jpg" title="Embalagem para alimentos">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-para-alimentos-02.jpg" alt="Embalagem para alimentos" />

		</a>
	</div>


	<div class="col-md-4 mb-4">

		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas2.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas2.jpg" alt="Empresa" />


		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-para-alimento-solda-standup-pouch.jpg" title="Embalagem para alimento Solda StandUp Pouch">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-para-alimento-solda-standup-pouch.jpg" alt="Embalagem para alimento Solda StandUp Pouch" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas-geral.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas-geral.jpg" alt="Empresa" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/embalagem-para-vestuario.jpg" title="Embalagem para vestuário">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/embalagem-para-vestuario.jpg" alt="Embalagem para vestuário" />
		</a>
	</div>
	

	<div class="col-md-4 mb-4">

		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas-pallet.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas-pallet.jpg" alt="Empresa" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelope-coex-seguranca.jpg" title="Envelope Coex Segurança">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelope-coex-seguranca.jpg" alt="Envelope Coex Segurança" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/envelopes-segurancas-impressos.jpg" title="Envelope Seguranças Impressos">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/envelopes-segurancas-impressos.jpg" alt="Envelope Seguranças Impressos" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobina-tecnica.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobina-tecnica.jpg" alt="Empresa" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacola-para-alimentos.jpg" title="Sacola para alimentos">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacola-para-alimentos.jpg" alt="Sacola para alimentos" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/extrusao-material-bobina.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/extrusao-material-bobina.jpg" alt="Empresa" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacolas.jpg" title="Sacolas">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/extrusao-material-bobina.jpg" alt="Empresa" />
		</a>
	</div>



	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/extrusora.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/extrusora.jpg" alt="Empresa" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/extrusora-bobina-lisa.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/extrusora-bobina-lisa.jpg" alt="Empresa" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacolas-02.jpg" title="Sacolas">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacolas-02.jpg" alt="Sacolas" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/frota-propria.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/frota-propria.jpg" alt="Empresa" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>produtos/sacos-zip-metalizado.jpg" title="Sacos Zip Metalizado">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/sacos-zip-metalizado.jpg" alt="Sacos Zip Metalizado" />
		</a>
	</div>

	<div class="col-md-4 mb-4">

		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/impressao.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/impressao.jpg" alt="Empresa" />

		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/material-pp.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/produtos/saco-zip.jpg" alt="Saco Zip" />
		</a>
	</div>


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/material-pp.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/material-pp.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima2.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima2.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima3.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima3.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/organizacao-pallet.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/organizacao-pallet.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima.jpg" alt="Empresa" />
		</a>
	</div>

	<!-- End empresas img -->

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas2.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas2.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas-geral.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas-geral.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobinas-plasticas-pallet.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobinas-plasticas-pallet.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/bobina-tecnica.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/bobina-tecnica.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/extrusao-material-bobina.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/extrusao-material-bobina.jpg" alt="Empresa" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/extrusora.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/extrusora.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/extrusora-bobina-lisa.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/extrusora-bobina-lisa.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/frota-propria.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/frota-propria.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/impressao.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/impressao.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/material-pp.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/material-pp.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima2.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima2.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima3.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima3.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/organizacao-pallet.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/organizacao-pallet.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>empresa/materia-prima.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/empresa/materia-prima.jpg" alt="Empresa" />
		</a>
	</div>

	<!-- End empresas img -->


	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/corte-solda.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/servicos/corte-solda.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/corte-solda2.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/servicos/corte-solda2.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/corte-solda3.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>servicos/corte-solda3.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/empacotamento-auto.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>servicos/empacotamento-auto.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/empacotamento-auto2.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>servicos/empacotamento-auto2.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/empacotamento-auto3.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>servicos/empacotamento-auto3.jpg" alt="Empresa" />
		</a>
	</div>
	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/empacotamento-auto4.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>servicos/empacotamento-auto4.jpg" alt="Empresa" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/servico.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>servicos/servico.jpg" alt="Empresa" />
		</a>
	</div>

	<div class="col-md-4 mb-4">
		<a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>servicos/servicos.jpg" title="Empresa">
			<img class="img-fluid" src="<?=$caminhoGaleria?>servicos/servicos.jpg" alt="Empresa" />
		</a>
	</div>
</div>
<div class="d-flex align-items-center justify-content-center" id='portfolioPagination'></div>





