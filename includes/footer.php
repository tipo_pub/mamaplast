<footer id="footer" class="mt-0">
	<div class="container">
		<div class="row py-5">
			<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
				<h5 class="text-6 text-transform-none text-color-light ls-0 mb-3">Menu</h5>
				<ul class="lista-rodape">
					<?php $menuRodape = true; $menuTopo = false; include "includes/menu.php";?>
				</ul>
			</div>

			<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
				<h5 class="text-6 text-transform-none text-color-light ls-0 mb-3">Contato</h5>
				<p class="text-4 mb-4"><a target="_blank" href="<?=$linkHorario;?>" title="Veja a <?=$nomeEmpresa;?> no Google Maps!"> <?=$endereco;?> <br /> <?=$cidade;?> - CEP: <?=$cep;?></a></p>
				
				<?php echo isset($tel) && ($tel != '') ? '<p class="text-4 mb-0 pt-1">Telefone: <a href="'.$tellink.'" title="Telefone para Contato">'.$ddd.' '.$tel.'</a></p>' : ''; ?>

				<?php echo isset($tel2) && ($tel2 != '') ? '<p class="text-4 mb-0 pt-1">Telefone: <a href="'.$tel2link.'" title="Telefone para Contato">'.$ddd.' '.$tel2.'</a></p>' : ''; ?>

				<?php echo isset($whats) && ($whats != '') ? '<p class="text-4 mb-0 pt-1">Atendimento Online: <a href="'.$whatslink.'" title="Atendimento Online">'.$ddd.' '.$whats.'</a></p>' : ''; ?>

				<?php echo isset($email) && ($email != '') ? '<p class="text-4 mb-0 pt-1">Email: <a href="mailto:'.$email.'" title="Enviar e-mail para: '.$email.'">'.$email.'</a></p>' : ''; ?>
			</div>

			<div class="col-md-12 col-lg-4 mb-lg-0">
				<h5 class="text-6 text-transform-none text-color-light ls-0 mb-3">Localização</h5>
				<iframe src="<?=$linkIframeMapa?>" class="iframeMapa" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<div class="footer-copyright footer-copyright-style-2">
		<div class="container py-4">
			<div class="row">
				<div class="col-md-8 d-flex text-center text-lg-left mb-4 mb-lg-0">
					<p>Copyright &copy; <?php echo (date("Y") > '2019') ? "2019 - ".date("Y").'&nbsp;&nbsp;'.$nomeEmpresa : date("Y").'&nbsp;&nbsp;'.$nomeEmpresa; ?> | Todos os Direitos Reservados</p>
				</div>
				<div class="col-md-4 d-flex align-items-center justify-content-center">
					<p>Desenvolvido por &nbsp;<a target="_blank" href="<?=$author?>" title="<?=$creditos?>"><img src="<?=$logoTipoPub?>" style="width:20px" alt="<?=$creditos?>"></a></p>
				</div>
			</div>
		</div>
	</div>
	<?php include 'includes/btn-lateral.php' ;?>
</footer>


<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/jquery.vide.min.js"></script>
<script src="vendor/vivus/vivus.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>
<script src="js/mask.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<script src="https://www.google.com/recaptcha/api.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-72058135-63"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-72058135-63');
</script>

</body>

</html>
