<section class="section section-light border-0 p-0">
    <div class="container">
        <div class="heading-pages-sub">
            <h2><?=$tituloGaleria?></h2>
        </div>

        <input type='hidden' id='current_page' />
        <input type='hidden' id='show_per_page' />

        <div class="row contGaleria lightbox" id='content' data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>galeria-01.jpg" title="Project Image 01">
                    <img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/galeria-01.jpg" alt="Project Image 01" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>galeria-02.jpg" title="Project Image 02">
                    <img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/galeria-02.jpg" alt="Project Image 02" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>galeria-03.jpg" title="Project Image 03">
                    <img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/galeria-03.jpg" alt="Project Image 03" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>galeria-04.jpg" title="Project Image 04">
                    <img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/galeria-04.jpg" alt="Project Image 04" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>galeria-05.jpg" title="Project Image 05">
                    <img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/galeria-05.jpg" alt="Project Image 05" />
                </a>
            </div>
            <div class="col-md-4 mb-4">
                <a class="img-thumbnail img-thumbnail-no-borders d-block img-thumbnail-hover-icon" href="<?=$caminhoGaleria?>galeria-06.jpg" title="Project Image 06">
                    <img class="img-fluid" src="<?=$caminhoGaleria?>thumbs/galeria-06.jpg" alt="Project Image 06" />
                </a>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-center" id='portfolioPagination'></div>
    </div>
</section>