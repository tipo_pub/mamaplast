<?php
if(!empty($menuTopo)){
	$classe_dropdown = 'dropdown';
	$classe_item = $classe_dropdown.'-item';
	$classe_toggle = $classe_dropdown.'-toggle';
	$classe_submenu =  $classe_dropdown.'-menu';
	$LI_mapa_site = false;
} elseif(!empty($menuRodape)) {
	$classe_dropdown = '';
	$classe_item = 'text-4 link-hover-style-1';
	$classe_toggle = '';
	$classe_submenu =  '';
	$LI_mapa_site = true;
	$submenu_mapa_site = false;
} elseif(!empty($menuMapaSite)) {
	$classe_dropdown = '';
	$classe_item = '';
	$classe_toggle = '';
	$classe_submenu =  '';
	$LI_mapa_site = false;
	$submenu_mapa_site = true;
}
?>

<li><a href="<?=$url;?>" class="<?=$classe_item?>" title="Home">Home</a></li>
<li class="<?=$classe_dropdown?>"><a href="<?=$url;?>institucional" class="<?=$classe_item.' '.$classe_toggle?>" title="Institucional">Institucional</a>
	<!-- <ul class="<?=$classe_submenu?>">
		<li><a href="<?=$url;?>portfolio" class="<?=$classe_item?>" title="Portfolio">Portfolio</a></li>
		<li><a href="<?=$url;?>parceiros" class="<?=$classe_item?>" title="Parceiros">Parceiros</a></li>
		<li><a href="<?=$url;?>clientes" class="<?=$classe_item?>" title="Clientes">Clientes</a></li>
	</ul> -->
</li>
<li><a href="<?=$url;?>produtos" class="<?=$classe_item?>" title="Produtos">Produtos</a></li>
<li><a href="<?=$url;?>servicos" class="<?=$classe_item?>" title="Serviços">Serviços</a></li>
<li><a href="<?=$url;?>galeria-fotos" class="<?=$classe_item?>" title="Galeria de Fotos">Galeria de Fotos</a></li>
<?php if(empty($menuRodape)) {?>
<li><a href="<?=$url;?>contato" class="<?=$classe_item?>" title="Contato">Contato</a></li>
<?php } if(!empty($LI_mapa_site)) {?>
</ul>
<ul class="lista-rodape">
	<li><a href="<?=$url;?>contato" class="<?=$classe_item?>" title="Contato">Contato</a></li>
	<li><a href="<?=$url;?>mapa-site" class="<?=$classe_item?>" title="Mapa do Site">Mapa do Site</a></li>
<?php } elseif(!empty($submenu_mapa_site)) {?>
</ul>
<ul class="lista-mapa-site">
	<li><a href="<?=$url;?>mapa-site" class="<?=$classe_item?>" title="Outros Produtos">Outros Produtos</a>
		<ul>
			<?php
			$keys = array_rand($menu, count($menu));

			$i = 0;
			foreach ($keys as $link) {
				$palavra = $menu[$link];
				echo "<li><a href=\"$url$link\" title=\"$palavra\">$palavra</a></li>";

				$i++;
			}
			?>
		</ul>
	</li>
<?php }?>