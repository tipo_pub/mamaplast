<?php if(isset($linkPagina) && ($linkPagina == 'index') || ($linkPagina = '')) {?>

	<div class="slider-container rev_slider_wrapper" style="height: 100vh;">
		<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'fullscreen', 'delay': 8000, 'gridwidth': 1140, 'gridheight': 800, 'responsiveLevels': [4096,1200,992,500]}">
			<ul>
				<li class="slide-overlay" data-transition="fade">
					<img src="<?=$caminhoBanners?>banner-06.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />

					<div class="tp-caption text-color-light font-weight-normal" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-105']" data-start="700" data-fontsize="['16','16','16','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;"></div>

					<h1 class="tp-caption font-weight-extra-bold text-color-light negative-ls-1 text-uppercase" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-30','-30','-30','-30']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']"><img src="<?=$logo?>" alt="<?=$nomeEmpresa?>" /> <span class="text-2">31 Anos</span></h1>

					<div class="tp-caption font-weight-light ws-normal text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.03,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['53','53','53','105']" data-width="['530','530','530','1100']" data-fontsize="['18','18','18','40']" data-lineheight="['26','26','26','45']" style="color: #b5b5b5;">Sinônimo de <strong class="text-color-light">Parceria</strong></div>

					<a class="tp-caption btn btn-primary btn-rounded font-weight-semibold" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"opacity:0;y:50%;","to":"o:1;y:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-hash data-hash-offset="85" href="<?=$url;?>institucional" data-x="center" data-hoffset="0" data-y="center" data-voffset="['133','133','133','255']" data-whitespace="nowrap" data-fontsize="['14','14','14','33']" data-paddingtop="['15','15','15','40']" data-paddingright="['45','45','45','110']" data-paddingbottom="['15','15','15','40']" data-paddingleft="['45','45','45','110']">Saiba Mais</a>
				</li>

				<li class="slide-overlay" data-transition="fade">
					<img src="<?=$caminhoBanners?>banner-07.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />

					<img src="<?=$caminhoBanners?>banner-08.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />

					<div class="tp-caption text-color-light font-weight-normal" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-105']" data-start="700" data-fontsize="['16','16','16','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;"></div>

					<h2 class="tp-caption font-weight-extra-bold text-color-light negative-ls-1 text-uppercase" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-30','-30','-30','-30']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">Sacos Personalizados</h2>

					<div class="tp-caption font-weight-light ws-normal text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.03,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['53','53','53','105']" data-width="['530','530','530','1100']" data-fontsize="['18','18','18','40']" data-lineheight="['26','26','26','45']" style="color: #b5b5b5;">Sacos - Sacolas - Valvulados - Zip Lock - Laminados - Metalizados - Microperfurados ou Furos Especiais - Oxibiodegradável</div>

					<a class="tp-caption btn btn-primary btn-rounded font-weight-semibold" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"opacity:0;y:50%;","to":"o:1;y:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-hash data-hash-offset="85" href="<?=$url;?>produtos" data-x="center" data-hoffset="0" data-y="center" data-voffset="['133','133','133','255']" data-whitespace="nowrap" data-fontsize="['14','14','14','33']" data-paddingtop="['15','15','15','40']" data-paddingright="['45','45','45','110']" data-paddingbottom="['15','15','15','40']" data-paddingleft="['45','45','45','110']">Saiba Mais</a>
				</li>

				<li class="slide-overlay" data-transition="fade">
					<img src="<?=$caminhoBanners?>banner-07.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />


					<div class="tp-caption text-color-light font-weight-normal" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-105']" data-start="700" data-fontsize="['16','16','16','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;"></div>

					<h2 class="tp-caption font-weight-extra-bold text-color-light negative-ls-1 text-uppercase" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-30','-30','-30','-30']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">Bobinas Plásticas</h2>

					<div class="tp-caption font-weight-light ws-normal text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.03,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['53','53','53','105']" data-width="['530','530','530','1100']" data-fontsize="['18','18','18','40']" data-lineheight="['26','26','26','45']" style="color: #b5b5b5;">PEBD - PEAD - Stretch - Reciclada</div>

					<a class="tp-caption btn btn-primary btn-rounded font-weight-semibold" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"opacity:0;y:50%;","to":"o:1;y:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-hash data-hash-offset="85" href="<?=$url;?>produtos" data-x="center" data-hoffset="0" data-y="center" data-voffset="['133','133','133','255']" data-whitespace="nowrap" data-fontsize="['14','14','14','33']" data-paddingtop="['15','15','15','40']" data-paddingright="['45','45','45','110']" data-paddingbottom="['15','15','15','40']" data-paddingleft="['45','45','45','110']">Saiba Mais</a>
				</li>

				<li class="slide-overlay" data-transition="fade">
					<img src="<?=$caminhoBanners?>banner-09.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />
					<div class="tp-caption text-color-light font-weight-normal" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-105']" data-start="700" data-fontsize="['16','16','16','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;"></div>

					<h2 class="tp-caption font-weight-extra-bold text-color-light negative-ls-1 text-uppercase" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-30','-30','-30','-30']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">Envelopes de Segurança</h2>

					<div class="tp-caption font-weight-light ws-normal text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.03,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['53','53','53','105']" data-width="['530','530','530','1100']" data-fontsize="['18','18','18','40']" data-lineheight="['26','26','26','45']" style="color: #b5b5b5;">Com Fitas Invioláveis, Sistema VOID, Número de Série, Coextrusados</div>

					<a class="tp-caption btn btn-primary btn-rounded font-weight-semibold" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"opacity:0;y:50%;","to":"o:1;y:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-hash data-hash-offset="85" href="<?=$url;?>produtos" data-x="center" data-hoffset="0" data-y="center" data-voffset="['133','133','133','255']" data-whitespace="nowrap" data-fontsize="['14','14','14','33']" data-paddingtop="['15','15','15','40']" data-paddingright="['45','45','45','110']" data-paddingbottom="['15','15','15','40']" data-paddingleft="['45','45','45','110']">Saiba Mais</a>
				</li>

				<li class="slide-overlay" data-transition="fade">
					<img src="<?=$caminhoBanners?>banner-05.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />

					<div class="tp-caption text-color-light font-weight-normal" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-105']" data-start="700" data-fontsize="['16','16','16','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;"></div>

					<h2 class="tp-caption font-weight-extra-bold text-color-light negative-ls-1 text-uppercase" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-y="center" data-voffset="['-30','-30','-30','-30']" data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">Serviços</h2>

					<div class="tp-caption font-weight-light ws-normal text-center" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.03,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]' data-x="center" data-y="center" data-voffset="['53','53','53','105']" data-width="['530','530','530','1100']" data-fontsize="['18','18','18','40']" data-lineheight="['26','26','26','45']" style="color: #b5b5b5;">Impressão Flexográfica até 6 cores corte e solda - Solda Pouch - Extrusão</div>

					<a class="tp-caption btn btn-primary btn-rounded font-weight-semibold" data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"opacity:0;y:50%;","to":"o:1;y:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-hash data-hash-offset="85" href="<?=$url;?>produtos" data-x="center" data-hoffset="0" data-y="center" data-voffset="['133','133','133','255']" data-whitespace="nowrap" data-fontsize="['14','14','14','33']" data-paddingtop="['15','15','15','40']" data-paddingright="['45','45','45','110']" data-paddingbottom="['15','15','15','40']" data-paddingleft="['45','45','45','110']">Saiba Mais</a>
				</li>

				
			</ul>
		</div>
	</div>

<?php } else { ?>
	<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url(<?php echo $caminhoBanners;?>banner-01.jpg);">
		<div class="container">
			<div class="row mt-5">
				<div class="col-md-12 align-self-center p-static order-1 text-center">
					<h1 class="font-Pattaya"><?=$title?></h1>
				</div>
				<div class="col-md-12 align-self-center order-2 mt-4">
					<ul class="breadcrumb breadcrumb-light d-block text-center">
						<?=breadcrumb()?>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>