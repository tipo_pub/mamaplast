<?php include "includes/geral.php";?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>

	<!-- Site Desenvolvido por <?php echo $creditos; ?> / <?php echo $author; ?> -->
	<?php /* Basic */?>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$nomeEmpresa;?> | <?=$title;?></title>
	<meta name="title" content="<?=$title;?>" />
	<meta name="author" content="<?=$author;?>" />
	<meta name="description" content="<?=$description;?>" />
	<meta name="keywords" content="<?=$title;?>, <?=$keywords;?>" />
	<meta name="robots" content="<?=$infoRobots;?>" />

	<?php echo $geolocation; ?>

	<?php /*SEO */?>
	<meta name="language" content="pt-br" />
	<meta name="copyright" content="<?=$nomeEmpresa . " " . $slogan?>" />
	<meta name="distribution" content="global" />
	<meta name="audience" content="all" />
	<meta name="url" content="<?=$canonical;?>" />
	<meta name="classification" content="<?=$ramo;?>" />
	<meta name="category" content="<?=$ramo;?>" />
	<meta name="Page-Topic" content="<?=$title . " - " . $nomeEmpresa;?>" />
	<meta name="rating" content="general" />
	<meta name="fone" content="<?=$tel . "|" . $tel2 . "|" . $tel3;?>" />
	<meta name="city" content="<?=$cidade;?>" />
	<meta name="country" content="Brasil" />
	<meta property="publisher" content="<?=$creditos;?>" />

	<?php /* Favicon */?>
	<link rel="shortcut icon" href="<?=$favicon;?>" type="image/x-icon" />
	<link rel="apple-touch-icon" href="<?=$faviconApple;?>" />
	
	<?php /*Google */?>
	<link rel="canonical" href="<?=$canonical;?>" />
	<meta name="googlebot" content="<?=$infoRobots;?>" />
	<meta name="geo.placename" content="Brasil" />
	<meta name="geo.region" content="<?=$cidade;?>" />
	<meta itemprop="name" content="<?=$nomeEmpresa;?>" />
	<meta itemprop="description" content="<?=$description;?>" />
	<meta itemprop="image" content="<?=$logo;?>" />

	<?php /*Twitter */?>
	<meta name="twitter:card" content="<?=$logo;?>" />
	<meta name="twitter:site" content="<?=$canonical;?>" />
	<meta name="twitter:title" content="<?=$title . " - " . $nomeEmpresa;?>" />
	<meta name="twitter:description" content="<?=$description;?>" />
	<meta name="twitter:creator" content="<?=$UserTwitter;?>" />
	<meta name="twitter:image:src" content="<?=$logo;?>" />

	<?php /*Facebook */?>
	<meta property="og:title" content="<?=$title . " - " . $nomeEmpresa;?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?=$canonical;?>" />
	<meta property="og:site_name" content="<?=$nomeEmpresa;?>" />
	<meta property="og:author" content="<?=$nomeEmpresa;?>" />
	<meta property="og:locale" content="pt_BR" />
	<meta property="og:region" content="Brasil" />
	<meta property="og:image" content="<?=$logo;?>" />
	<meta property="og:image:type" content="image/jpg" />
	<meta property="og:image:width" content="250" />
	<meta property="og:image:height" content="250" />
	<meta property="og:description" content="<?=$description;?>" />
	<meta property="fb:admins" content="<?=$CodFanpage;?>" />

	<?php /* Pré-Load de Domínios Externos */?>
	<link rel='dns-prefetch' href='//www.google.com' />

	<?php /* Web Fonts  */?>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css"
>
	<link href="https://fonts.googleapis.com/css?family=Pattaya&display=swap" rel="stylesheet" type="text/css">
	
	<?php /* Vendor CSS */?>
	<link rel="stylesheet" href="<?=$url;?>vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/animate/animate.min.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/magnific-popup/magnific-popup.min.css">

	<?php /* Theme CSS */?>
	<link rel="stylesheet" href="<?=$url;?>css/theme.css">
	<link rel="stylesheet" href="<?=$url;?>css/theme-elements.css">
	<link rel="stylesheet" href="<?=$url;?>css/theme-blog.css">
	<link rel="stylesheet" href="<?=$url;?>css/theme-shop.css">

	<?php /* Current Page CSS */?>
	<link rel="stylesheet" href="<?=$url;?>vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="<?=$url;?>vendor/rs-plugin/css/navigation.css">

	<?php /* Skin CSS */?>
	<link rel="stylesheet" href="<?=$url;?>css/skin-corporate-7.css">

	<?php /* Theme Custom CSS */?>
	<link rel="stylesheet" href="<?=$url;?>css/custom.css">

	<?php /* Head Libs */?>
	<script src="<?=$url;?>vendor/modernizr/modernizr.min.js"></script>