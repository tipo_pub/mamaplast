<?php
$title = 'Clientes';
$description = 'Conheça nossos clientes';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4">
	<div class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2 class="font-weight-normal text-primary">Conheça nossos Clientes</h2>
	</div>


	
	<div class="content-grid my-5">




		<div class="row content-grid-row">
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-01.png" alt="Cliente - 01" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-02.png" alt="Cliente - 02" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-03.png" alt="Cliente - 03" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-04.png" alt="Cliente - 04" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-05.png" alt="Cliente - 05" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-06.png" alt="Cliente - 06" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-07.png" alt="Cliente - 07" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-08.png" alt="Cliente - 08" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-01.png" alt="Cliente - 01" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-02.png" alt="Cliente - 02" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-03.png" alt="Cliente - 03" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-04.png" alt="Cliente - 04" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-05.png" alt="Cliente - 05" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-06.png" alt="Cliente - 06" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-07.png" alt="Cliente - 07" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoClientes?>cliente-08.png" alt="Cliente - 08" />
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'includes/footer.php' ;?>