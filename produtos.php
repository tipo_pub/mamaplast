<?php
$title = 'Produtos';
$description = 'Embalagem para VestuÃ¡rio, Embalagem para Alimentos, Envelopes, Sacolas, Sacos de Lixo,  Sacos ZIP';
$keywords = 'Embalagens plÃ¡sticas para congelados, Embalagens plÃ¡sticas para lavanderia, Embalagens plÃ¡sticas para produtos congelados, Embalagens plÃ¡sticas para produtos quÃ­micos, Envelope AWB para nota fiscal, Envelope plÃ¡stico com lacre de seguranÃ§a, 
Envelope plÃ¡stico de seguranÃ§a, Envelopes coextrusados';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4">
	<div class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2 class="font-weight-normal text-primary">Conheça nossos Produtos</h2>
	</div>

	<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
		<li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active" href="#">Ver Todos</a></li>

		<li class="nav-item" data-option-value=".embalagem-vestuarios"><a class="nav-link text-1 text-uppercase" href="#">Embalagem para Vestuário</a></li>
		<li class="nav-item" data-option-value=".embalagem-alimentos"><a class="nav-link text-1 text-uppercase" href="#">Embalagem para Alimentos</a></li>
		<li class="nav-item" data-option-value=".envelopes"><a class="nav-link text-1 text-uppercase" href="#">Envelopes</a></li>
		<li class="nav-item" data-option-value=".sacolas"><a class="nav-link text-1 text-uppercase" href="#">Sacolas</a></li>
		<li class="nav-item" data-option-value=".sacos-lixo"><a class="nav-link text-1 text-uppercase" href="#">Sacos de Lixo</a></li>
		<li class="nav-item" data-option-value=".sacos-zip"><a class="nav-link text-1 text-uppercase" href="#">Sacos ZIP</a></li>
	

	</ul>

	<div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
		<div class="row pt-5 pb-0 justify-content-center portfolio-list sort-destination contGaleria lightbox" data-sort-id="portfolio" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">


			<?php /* - Embalagem para VestuÃ¡rio - */?>
			<div class="col-sm-6 col-lg-3 isotope-item embalagem-vestuarios">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>embalagem-vestuario.jpg" title="Embalagem para VestuÃ¡rio">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>embalagem-vestuario.jpg" class="img-fluid border-radius-0" alt="Embalagem para VestuÃ¡rio">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<?php /* - Embalagem para Alimentos - */?>
			<div class="col-sm-6 col-lg-3 isotope-item embalagem-alimentos">
				
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>embalagem-alimentos-01.jpg" title="Embalagem para Alimentos - 01">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>embalagem-alimentos-01.jpg" class="img-fluid border-radius-0" alt="Embalagem para Alimentos - 01">
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-3 isotope-item embalagem-alimentos">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>embalagem-alimentos-02.jpg" title="Embalagem para Alimentos - 02">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>embalagem-alimentos-02.jpg" class="img-fluid border-radius-0" alt="Embalagem para Alimentos - 02">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item embalagem-alimentos">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>embalagem-alimentos-03.jpg" title="Embalagem para Alimentos - 03">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>embalagem-alimentos-03.jpg" class="img-fluid border-radius-0" alt="Embalagem para Alimentos - 03">
							</span>
						</span>
					</a>
				</div>
			</div>


			<div class="col-sm-6 col-lg-3 isotope-item embalagem-alimentos">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>embalagem-alimentos-04.jpg" title="Embalagem para Alimentos - 04">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>embalagem-alimentos-04.jpg" class="img-fluid border-radius-0" alt="Embalagem para Alimentos - 04">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<?php /* - Envelopes - */?>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-coex-seguranca.jpg" title="Envelope COEX SeguranÃ§a">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-coex-seguranca.jpg" class="img-fluid border-radius-0" alt="Envelope COEX SeguranÃ§a">

							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelopes-segurancas-impressos.jpg" title="Envelopes SeguranÃ§as Impressos">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelopes-segurancas-impressos.jpg" class="img-fluid border-radius-0" alt="Envelopes SeguranÃ§as Impressos">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelopes-datados-seriados-01.jpg" title="Envelopes Datados e Seriados - 01">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelopes-datados-seriados-01.jpg" class="img-fluid border-radius-0" alt="Envelopes Datados e Seriados - 01">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelopes-datados-seriados-02.jpg" title="Envelopes Datados e Seriados - 02">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelopes-datados-seriados-02.jpg" class="img-fluid border-radius-0" alt="Envelopes Datados e Seriados - 02">
							</span>
						</span>
					</a>
				</div>
			</div>	
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelopes-datados-seriados-03.jpg" title="Envelopes Datados e Seriados - 03">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelopes-datados-seriados-03.jpg" class="img-fluid border-radius-0" alt="Envelopes Datados e Seriados - 03">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-01.jpg" title="Envelope SeguranÃ§a - 01">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-01.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 01">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-02.jpg" title="Envelope SeguranÃ§a - 02">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-02.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 02">
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-03.jpg" title="Envelope SeguranÃ§a COEX - 03">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-03.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a COEX - 03">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-04.jpg" title="Envelope SeguranÃ§a COEX - 04">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-04.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a COEX - 04">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-05.jpg" title="Envelope SeguranÃ§a - 05">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-05.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 05">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-06.jpg" title="Envelope SeguranÃ§a - 06">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-06.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 06">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-07.jpg" title="Envelope SeguranÃ§a - 07">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-07.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 07">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-datados-coex-01.jpg" title="Envelope SeguranÃ§a - 01">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-datados-coex-01.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 01">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-datados-coex-02.jpg" title="Envelope SeguranÃ§a - 02">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-datados-coex-02.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 02">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>envelope-seguranca-datados-coex-03.jpg" title="Envelope SeguranÃ§a - 03">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>envelope-seguranca-datados-coex-03.jpg" class="img-fluid border-radius-0" alt="Envelope SeguranÃ§a - 03">

								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<?php /* - Sacolas - */?>
			<div class="col-sm-6 col-lg-3 isotope-item sacolas">
				<div class="portfolio-item">
					<a href="<?=$caminhoProdutos?>sacolas-01.jpg" title="Sacolas - 01">
						<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$produtosThumbs?>sacolas-01.jpg" class="img-fluid border-radius-0" alt="Sacolas - 01">
								</span>
							</span>
						</a>
					</div>
				</div>



								
								<div class="col-sm-6 col-lg-3 isotope-item sacolas">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>sacolas-02.jpg" title="Sacolas - 02">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>sacolas-02.jpg" class="img-fluid border-radius-0" alt="Sacolas - 02">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3 isotope-item sacolas">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>sacolas-03.jpg" title="Sacolas - 03">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>sacolas-03.jpg" class="img-fluid border-radius-0" alt="Sacolas - 03">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3 isotope-item sacolas">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>sacolas-04.jpg" title="Sacolas - 04">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>sacolas-04.jpg" class="img-fluid border-radius-0" alt="Sacolas - 04">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3 isotope-item sacolas">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>sacolas-05.jpg" title="Sacolas - 05">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>sacolas-05.jpg" class="img-fluid border-radius-0" alt="Sacolas - 05">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>

								<?php /* - Sacos de Lixo - */?>
								<div class="col-sm-6 col-lg-3 isotope-item sacos-lixo">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>sacos-lixo.jpg" title="Sacos de Lixo">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>sacos-lixo.jpg" class="img-fluid border-radius-0" alt="Sacos de Lixo">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>

								<?php /* - Sacos de Lixo - */?>
								<div class="col-sm-6 col-lg-3 isotope-item sacos-zip">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>saco-zip-01.jpg" title="Sacos ZIP - 01">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>saco-zip-01.jpg" class="img-fluid border-radius-0" alt="Sacos ZIP - 01">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3 isotope-item sacos-zip">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>saco-zip-04.jpg" title="Sacos ZIP - 02">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>saco-zip-04.jpg" class="img-fluid border-radius-0" alt="Sacos ZIP - 02">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3 isotope-item sacos-zip">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>saco-zip-02.jpg" title="Sacos Metalizados Solda Pouch">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>saco-zip-02.jpg" class="img-fluid border-radius-0" alt="Sacos Metalizados Solda Pouch">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="col-sm-6 col-lg-3 isotope-item sacos-zip">
									<div class="portfolio-item">
										<a href="<?=$caminhoProdutos?>saco-zip-03.jpg" title="Sacos ZIP Metalizados">
											<span class="thumb-info thumb-info-lighten border-radius-0 mb-3">
												<span class="thumb-info-wrapper border-radius-0">
													<img src="<?=$produtosThumbs?>saco-zip-03.jpg" class="img-fluid border-radius-0" alt="Sacos ZIP Metalizados">

													<span class="thumb-info-action">
														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

			<?php include 'includes/footer.php' ;?>