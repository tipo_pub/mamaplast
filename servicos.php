<?php
$title = 'Serviços';
$description = 'Extrusão "Balão", Impressão Flexográfica em até 6 Cores, Acabamento em Corte e Solda';
$keywords = 'Extrusão "Balão", Impressão Flexográfica em até 6 Cores, Acabamento em Corte e Solda';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4">
	<div class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2 class="font-weight-normal text-primary">Conheça nossos Serviços</h2>
	</div>
</div>
<div class="container-fluid">
	<div class="row align-items-center bg-color-grey">
		<div class="col-lg-6 p-0">
			<section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="<?=$caminhoBanners?>banner-02.jpg" style="min-height: 315px;">
			</section>
		</div>
		<div class="col-lg-6 p-0">
			<section class="section section-no-border h-100 m-0">
				<div class="row m-0">
					<div class="col-half-section col-half-section-left">
						<div class="overflow-hidden">
							<!-- <h4 class="mb-0 appear-animation" data-appear-animation="maskUp"><a href="#" class="text-4 font-weight-bold pt-2 d-block text-dark text-decoration-none pb-1">Extrusão "Balão"</a></h4> -->
							<h4 class="mb-0 appear-animation" data-appear-animation="maskUp">Extrusão "Balão"</h4>
						</div>
						<div class="overflow-hidden mb-2">
							<!-- <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p> -->
						</div>
						<!-- <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus pulvinar.</p> -->
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="row align-items-center bg-color-grey">
		<div class="col-lg-6 order-2 order-lg-1 p-0">
			<section class="section section-no-border h-100 m-0">
				<div class="row justify-content-end m-0">
					<div class="col-half-section col-half-section-right custom-text-align-right">
						<div class="overflow-hidden"><!-- 
							<h4 class="mb-0appear-animation" data-appear-animation="maskUp"><a href="#" class="text-4 font-weight-bold pt-2 d-block text-dark text-decoration-none pb-1">Impressão Flexográfica em até 6 Cores</a></h4> -->
							<h4>Impressão Flexográfica em até 6 Cores</h4>
						</div>
						<div class="overflow-hidden mb-2">
							<!-- <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p> -->
						</div>
						<!-- <p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus pulvinar.</p> -->
					</div>
				</div>
			</section>
		</div>
		<div class="col-lg-6 order-1 order-lg-2 p-0">
			<section class="parallax section section-parallax h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="<?=$caminhoBanners?>banner-03.jpg" style="min-height: 315px;">
			</section>
		</div>
	</div>
	<div class="row align-items-center bg-color-grey">
		<div class="col-lg-6 p-0">
			<section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="<?=$caminhoBanners?>banner-04.jpg" style="min-height: 315px;">
			</section>
		</div>
		<div class="col-lg-6 p-0">
			<section class="section section-no-border h-100 m-0">
				<div class="row m-0">
					<div class="col-half-section col-half-section-left">
						<div class="overflow-hidden">
							<!-- 							<h4 class="mb-0 appear-animation" data-appear-animation="maskUp"><a href="#" class="text-4 font-weight-bold pt-2 d-block text-dark text-decoration-none pb-1">Acabamento em Corte e Solda</a></h4> -->
							<h4>Acabamento em Corte e Solda</h4>
						</div>
						<div class="overflow-hidden mb-2">
							<!-- <p class="mb-0 lead text-4 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p> -->
						</div>

						<!-- 	<p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus pulvinar.</p> -->

					<!-- 	<p class="text-2 mb-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus pulvinar.</p> -->

					</div>
				</div>
			</section>
		</div>
	</div>
</div>

<?php include 'includes/footer.php' ;?>