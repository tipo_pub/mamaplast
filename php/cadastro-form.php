<?php
if(isset($_POST['submit']) && !empty($_POST['submit'])){
	/* Pego a validação do Captcha feita pelo usuário */
	$recaptcha_response = $_POST['g-recaptcha-response'];

	/* Verifico se foi feita a postagem do Captcha */
	if(isset($recaptcha_response)){
		function my_file_get_contents( $site_url ){
			$ch = curl_init();
			$timeout = 5; 
			curl_setopt ($ch, CURLOPT_URL, $site_url);
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			ob_start();
			curl_exec($ch);
			curl_close($ch);
			$file_contents = ob_get_contents();
			ob_end_clean();
			return $file_contents;
		}

		/* Valido se a ação do usuário foi correta junto ao google */
		$answer = json_decode(
			my_file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response'])
		);

		/* Se a ação do usuário foi correta executo o restante do meu formulário */
		if($answer->success) {
			require_once 'lib/defaultsend.php';
			$campos = $_POST;
			reset($campos);

			$data = date("H:i d-m-y");

			$nomeremetente = $nomeEmpresa;

			if (!empty($_FILES['files'])) {
				$assunto = 'Trabalhe Conosco Via Site - '.$nomeremetente;
				$checkAnexo = $_FILES['files'];
				$remetente = "comercial@mainflame.com.br";
				$remetente = "luiz.tipopub@gmail.com";
			} elseif (!empty($_POST['form_orcamento'])) {
				$assunto = 'Orçamento Via Site - '.$nomeremetente;
				$checkAnexo = NULL;
				$remetente = "comercial@mainflame.com.br";
				$remetente = "luiz.tipopub@gmail.com";
			}else {
				$assunto = 'Contato Via Site - '.$nomeremetente;
				$checkAnexo = NULL;
				$remetente = "comercial@mainflame.com.br";
				$remetente = "luiz.tipopub@gmail.com";
			}

			$destinatario = $remetente;

			$valores = "";

			while (list($key, $val) = each($campos)) {
				if ($key != 'submit' && $val != '' && $key != 'tipo_form' && $key != 'g-recaptcha-response' && $key != 'form_orcamento'){
					$substituir = array("Email");
					$mudar = array("E-mail");

					$keyModif = str_replace('_', ' ', ucfirst($key));
					/*$keyModif = str_replace($substituir, $mudar, $keyModif);*/

					$valores .= "<li style='width: 530px; /*border-bottom: solid 1px #c0c0c0*/; padding: 5px 0 ;font: 16px Helvetica Neue,Arial,Helvetica,Geneva,sans-serif; color: #808080;margin: 20px 15px;'><strong>$keyModif:</strong> $val</li>";
				}
			}

			$valorCampos = $valores;

			$mensagemHTML =
			"<html>
			<head>
			<style type='text/css'>
			body {
				margin: 0px;
				font-family: Verdana;
				font-size: 16px;
				color: #808080;
			}
			h2 {
				text-transform: capitalize;
			}
			</style>
			</head>
			<body style='font-family: Verdana;color: #808080;background-color: #dedede;'>
			<div id='wrapper-email'>	
			<br /><br />

			<div class=\"campos\">
			<ul style='box-shadow: 0px 0 14px #716f6f;width: 600px;list-style-type: none; background: #fff; border-radius: 5px; color: #808080; margin: 50px; font: 16px Helvetica Neue,Arial,Helvetica,Geneva,sans-serif; box-shadow: 0px 0 14px #716f6f;'>

			<li style='width: 530px; text-align: center; padding: 20px 0;'><img src='". $logo . "'></li>

			<li><h3 style='border-bottom: solid 1px #808080; width: 530px;font-size: 18px; line-height: 24px; color: rgb(176,176,176); font-weight: bold; margin-top: 0px; margin-bottom: 18px; font-family: Helvetica Neue,Arial,Helvetica,Geneva,sans-serif;'>$assunto</h3></li>

			$valorCampos

			<li style='width: 530px; border-bottom: solid 1px #c0c0c0; text-align: right; padding: 5px 0 ;font: 16px Helvetica Neue,Arial,Helvetica,Geneva,sans-serif;'>$data</li>

			<li style='width: 530px;background: #d2d2d2; padding: 10px 0; text-align: right; margin: 5px 0 5px 15px;'><a target='_blank' href='http://www.tipopublicidade.com.br' style='margin-right: 10px;'><img src='http://www.mainflame.com.br/img/tipo.png' style=' width: 20px;' ></li >  

			</ul>
			<br /><br />
			</div>
			</div>
			</body>
			</html>";

			if(defaultsend($remetente, $nomeremetente, $destinatario, $assunto, $mensagemHTML, $checkAnexo)){
				echo '<script language="JavaScript">location.href="'.$url.'enviado.php"</script>';
			} else{
				echo '<script language="JavaScript">location.href="'.$url.'falha.php"</script>';
			}
		}
		/* Caso o Captcha não tenha sido validado - retorno uma mensagem de erro. */
		else {
			echo "Por favor, faça a verificação do captcha.";
		}
	}
}
?>
