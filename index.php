<?php
$title = 'Home';
$description = 'Nossa linha de produtos atende empresas em diversos segmentos. Desde 1988 a MamaPlast é tradicional e referência no segmento de embalagens flexíveis.';
$keywords = 'Bobinas para empacotamento automático, Bobinas plásticas personalizadas, Bobinas plásticas preço, Capa plástica para pallet, Cobertura para cadáver, Embalagem para indústria química, Embalagem termo retrátil, Embalagens com hot melt';
include 'includes/head.php';
include 'includes/header.php';
?>

<section class="parallax section my-0 section-parallax" style="padding: 100px 0;" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="<?=$pastaImg?>img-parallax-home.jpg">
    <section class="call-to-action">
        <div class="container">
            <div class="row text-color-dark">
                <div class="col-sm-9 col-lg-9">
                    <div class="call-to-action-content">
                        <h3>Nossa <strong class="font-weight-extra-bold">linha de produtos</strong> atende empresas em diversos segmentos</h3>
                    </div>
                </div>
                <div class="col-sm-3 col-lg-3">
                    <div class="call-to-action-btn m-auto">
                        <a href="<?=$url?>produtos" class="btn btn-outline btn-rounded btn-primary btn-with-arrow text-uppercase" title="Confira nossa linha de produtos">Confira<span><i class="fas fa-chevron-right"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<section class="section section-primary border-0 py-0 my-0 appear-animation" data-appear-animation="fadeIn">
    <div class="container">
        <div class="row align-items-center justify-content-center justify-content-lg-between pb-5 pb-lg-0">
            <div class="col-lg-5 order-2 order-lg-1 pt-4 pt-lg-0 pb-5 pb-lg-0 mt-5 mt-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                <h2 class="font-weight-bold text-color-light text-uppercase text-7 mb-2">A Empresa</h2>

                <p class="text-3">Desde 1988 a MamaPlast é tradicional e referência no segmento de embalagens flexíveis. Com uma moderna estrutura e serviços de primeira linha, a MamaPlast oferece alta qualidade em embalagens PP, PEAD, COEX e Biodegradáveis, atendendo diversos segmentos, tais como: Industriais, Automotivos, Alimenticios, Farmacêuticos, Metalúrgicos, Laboratórios, entre outros. Além disso, ainda oferecemos atendimento exclusivo e personalizado, com o melhor prazo de entrega e ótimas facilidades de pagamento.</p>
                
                <a href="<?=$url?>institucional" class="btn btn-rounded btn-3d btn-light text-uppercase" title="Saiba mais sobre a <?=$nomeEmpresa?>">Saiba mais <i class="fa fa-chevron-right"></i></a>
            </div>
            <div class="col-9 offset-lg-1 col-lg-5 order-1 order-lg-2 scale-2">
                <img class="img-fluid box-shadow-3 my-2 border-radius" src="<?=$pastaImg?>img-empresa-home.jpg" alt="<?=$nomeEmpresa;?>">
            </div>
        </div>
    </div>
</section>

<section class="section border-0 my-0" style="padding: 100px 0;">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="row clearfix">
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 reverse appear-animation" data-appear-animation="fadeInRightShorter">
                            <div class="feature-box-icon">
                                <i class="icon-bag icons text-primary"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Sacos / Sacolas</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInLeftShorter">
                            <div class="feature-box-icon">
                                <i class="icon-handbag icons"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Embalagens para Alimentos</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 reverse appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                            <div class="feature-box-icon">
                                <i class="icon-envelope icons text-primary"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Envelopes</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">
                            <div class="feature-box-icon">
                                <i class="icon-bag icons text-primary"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-2">Embalagens para Vestuário</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col text-center">
                <a href="<?=$url?>produtos" class="btn btn-primary text-uppercase btn-px-5 py-3 font-weight-semibold text-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300" title="Saiba mais sobre os serviços da <?=$nomeEmpresa?>">Saiba Mais</a>
            </div>
        </div>
    </div>
</section>

<section class="section section-light border-0 p-0">
    <div class="container">
        <div class="heading heading-border heading-middle-border heading-middle-border-center">
            <h2 class="font-weight-bold text-primary">Galeria</h2>
        </div>
        <?php $qntPag = "contIndex"; include 'includes/galeria.php' ;?>
    </div>
</section>
    
<?php include 'includes/footer.php' ;?>