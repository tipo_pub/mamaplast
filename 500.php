<?php
$title			= 'Erro 500';
$description	= 'Erro 500';
$keywords		= '';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4">
	<section class="http-error">
		<div class="row justify-content-center">
			<div class="col text-center">
				<div class="http-error-main">
					<div class="heading heading-border heading-middle-border heading-middle-border-center">
						<h2 class="font-weight-bold text-primary">500</h2>
					</div>
					<p class="text-10 my-4">Oooops, página não encontrada!</p>
					<p class="lead"><a href="mapa-site"><button type="button" class="tp-caption btn btn-primary text-uppercase font-weight-bold btn-rounded"><i class="fa fa-arrow-right"></i> <span>Voltar para o site</span> <i class="fa fa-arrow-left"></i></button></a></p>
				</div>
			</div>	
		</div>
	</section>
</div>
<?php include "includes/footer.php";?>