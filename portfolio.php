<?php
$title = 'Portfólio';
$description = 'Veja nosso portfólio';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4">
	<div class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2 class="font-weight-normal text-primary">Portfólio de Produtos e Serviços</h2>
	</div>

	<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center mt-5" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
		<li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active" href="#">Mostrar Tudo</a></li>
		<li class="nav-item" data-option-value=".sacos"><a class="nav-link text-1 text-uppercase" href="#">Sacos</a></li>
		<li class="nav-item" data-option-value=".bobinas"><a class="nav-link text-1 text-uppercase" href="#">Bobinas</a></li>
		<li class="nav-item" data-option-value=".envelopes"><a class="nav-link text-1 text-uppercase" href="#">Envelopes</a></li>
		<li class="nav-item" data-option-value=".servicos"><a class="nav-link text-1 text-uppercase" href="#">Serviços</a></li>
	</ul>

	<div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
		<div class="row portfolio-list sort-destination" data-sort-id="portfolio">
			<div class="col-sm-6 col-lg-4 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item bobinas">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item sacos">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item bobinas">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item envelopes">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item sacos">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item servicos">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item bobinas">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item sacos">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item servicos">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

			<div class="col-sm-6 col-lg-4 isotope-item sacos">
				<div class="portfolio-item">
					<a href="portfolio-single-wide-slider.html">
						<span class="thumb-info thumb-info-lighten border-radius-0">
							<span class="thumb-info-wrapper border-radius-0">
								<img src="<?=$pastaImg?>img-empresa-home.jpg" class="img-fluid border-radius-0" alt="">

								<span class="thumb-info-title">
									<span class="thumb-info-inner">Nome do Cliente</span>
									<span class="thumb-info-type">Serviço Prestado</span>
								</span>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
								</span>
							</span>
						</span>
					</a>
				</div>
			</div>

		</div>
	</div>

</div>

<?php include 'includes/footer.php' ;?>