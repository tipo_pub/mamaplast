<?php
$title			= 'Falha ao Enviar Formulário';
$description	= 'Falha ao Enviar Formulário. Tente novamente mais tarde ou entre em contato pelos telefones disponíveis no site.';
$keywords		= '';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4">
	<section class="http-error">
		<div class="row justify-content-center">
			<div class="col text-center">
				<div class="http-error-main">
					<h2><i class="fa fa-exclamation-triangle fa-2 text-primary"></i></h2>
					<p class="text-10"><?=$title?></p>
					<p class="text-4 my-2">Tente novamente mais tarde ou entre em contato: <br />
						<?php
						/* Tel 1 */
						echo isset($tel) && ($tel != '') ? '<a href="'.$tellink.'" title="Telefone para Contato"><span>'.$ddd.' '.$tel.'</span></a>' : '';
						/* Tel 2 */
						echo isset($tel2) && ($tel2 != '') ? ' / <a href="'.$tel2link.'" title="Telefone para Contato"><span>'.$ddd.' '.$tel2.'</span></a>' : '';
						/* Whatsapp */
						echo isset($whats) && ($whats != '') ? ' / <a href="'.$whatslink.'" title="Atendimento Online"><span>'.$ddd.' '.$whats.'</span><i class="fab fa-whatsapp"></i></a>' : '';
						/* E-mail */
						echo isset($email) && ($email != '') ? ' / <a href="mailto:'.$email.'" title="Enviar e-mail para: '.$email.'"><span>'.$email.'</span></a>' : '';
						?>
					</p>
				</div>
			</div>	
		</div>
	</section>
</div>

<?php include "includes/footer.php";?>