<?php
$title = 'Parceiros';
$description = 'Conheça nossos parceiros';
$keywords = '';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4">
	<div class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2 class="font-weight-normal text-primary">Conheça nossos Parceiros</h2>
	</div>

	<div class="content-grid content-grid-dashed mt-5 mb-4">
		<div class="row content-grid-row">
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-01.png" alt="Parceiro - 01" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-02.png" alt="Parceiro - 02" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-03.png" alt="Parceiro - 03" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-04.png" alt="Parceiro - 04" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-05.png" alt="Parceiro - 05" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-06.png" alt="Parceiro - 06" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-07.png" alt="Parceiro - 07" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-08.png" alt="Parceiro - 08" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-01.png" alt="Parceiro - 01" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-02.png" alt="Parceiro - 02" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-03.png" alt="Parceiro - 03" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-04.png" alt="Parceiro - 04" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-05.png" alt="Parceiro - 05" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-06.png" alt="Parceiro - 06" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-07.png" alt="Parceiro - 07" />
				</div>
			</div>
			<div class="content-grid-item col-lg-3 text-center">
				<div class="p-4">
					<img class="img-fluid" src="<?=$caminhoParceiros?>parceiro-08.png" alt="Parceiro - 08" />
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'includes/footer.php' ;?>